package com.paragon.netapp.modules;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

public class AdminPage {

	public static void addUsers(WebDriver driver) throws InterruptedException {

		driver.findElement(By.xpath("//*[text()='Admin']")).click();

		driver.findElement(By.xpath("//*[text()='Native User Management']")).click();
		driver.findElement(By.xpath("//*[text()='Add User']")).click();

		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.name("firstName")).sendKeys("EnterFName");
		driver.findElement(By.name("lastName")).sendKeys("EnterLName");
		driver.findElement(By.name("password")).sendKeys("Enterpasskey");
		driver.findElement(By.name("confirmPassword")).sendKeys("Enterpasskey");
		driver.findElement(By.name("email")).sendKeys("entermailid@netapp.com");

		// Select item from dropDown List

		Thread.sleep(3000);
		// Select item from dropDown List
		WebElement list = driver.findElement(By.xpath("//*[@name='userRoleId']"));
		Select dropdownlist = new Select(list);
		Thread.sleep(3000);

		dropdownlist.selectByVisibleText("Migration Specialist");
		Select dropdown1 = new Select(driver.findElement(By.name("user.enabled")));
		dropdown1.selectByIndex(0);
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("/html/body/app-root/main/app-user-admin/app-adduser/form/div/div/div[3]/button"))
				.click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}
}