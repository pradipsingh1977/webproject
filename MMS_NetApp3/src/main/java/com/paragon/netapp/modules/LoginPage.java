package com.paragon.netapp.modules;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage

{
	public static void loginToApp(WebDriver driver, String username, String password) throws InterruptedException {
		driver.findElement(By.name("username")).clear();
		driver.findElement(By.name("username")).sendKeys(username);
		driver.findElement(By.id("pwd1")).clear();
		driver.findElement(By.id("pwd1")).sendKeys(password);
		Thread.sleep(2000);
		driver.findElement(By.xpath("//button[text()='Login']")).click();
		Thread.sleep(2000);
		//driver.findElement(By.xpath("//button[text()='Login']")).click();
		//driver.findElement(By.xpath("//*[@id=\"text-left\"]")).click();
	}

	public static void connectionItem(WebDriver driver) {

		driver.findElement(By.xpath("//*[text()='Connections']")).click();
		driver.findElement(By.xpath("//*[text()='Discover']")).click();
		driver.findElement(By.xpath("//*[text()='Metrics']")).click();
		driver.findElement(By.xpath("//*[text()='Scheduling']")).click();
		driver.findElement(By.xpath("//*[text()='Planning']")).click();
		driver.findElement(By.xpath("//*[text()='Execution']")).click();
		driver.findElement(By.xpath("//*[text()='Admin']")).click();
	}

	public static void logoutFromApp(WebDriver driver) {

		driver.findElement(By.xpath(".//*[text()='Logout']")).click();
	}
}