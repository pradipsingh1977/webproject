package com.paragon.netapp.tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.paragon.netapp.modules.AdminPage;
import com.paragon.netapp.modules.LoginPage;

public class loginTest {

	public WebDriver driver;

	String browser = "Chrome"; // browser can be brought from external properties file too
	String APPURL = "http://dv2-ls-cts-1:4200/";
	public WebDriverWait wait;

	@BeforeClass
	public void launchBrowser() throws Exception {
		if (browser.equalsIgnoreCase("Chrome")) {
			System.setProperty("webdriver.chrome.driver",
					".\\src\\main\\resources\\com\\paragon\\resources\\drivers\\chromedriver.exe");
			ChromeOptions cop = new ChromeOptions();
			cop.setAcceptInsecureCerts(true);

			driver = new ChromeDriver(cop);
			// driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			Thread.sleep(2000);

		} else if (browser.equalsIgnoreCase("firefox")) {
			driver = new FirefoxDriver();
		} else {
			driver = new InternetExplorerDriver();
		}
	}

	@BeforeMethod
	public void launchApp() {
		driver.get(APPURL);
	}
	// We can use data provider to tests - using apache poi or from DB

	// @Test(priority = 0)

	@Test
	public void TestCaseLogin() throws Exception {
		LoginPage.loginToApp(driver, "jdortee@corp.com", "Paraa");
		Thread.sleep(3000);

		// driver.switchTo().alert().accept();

		LoginPage.loginToApp(driver, "jdoe@corp.com", "123");
		Thread.sleep(2000);
		driver.manage().window().maximize();

		AdminPage.addUsers(driver);
		Thread.sleep(3000);
		driver.switchTo().alert().accept();
		
		driver.findElement(By.xpath("")).click();

	}

	@AfterClass
	public void closeBrowser() throws InterruptedException {

		LoginPage.logoutFromApp(driver);
		// Thread.sleep(5000);

		driver.close();
		driver.quit();
	}
}